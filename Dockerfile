FROM node:7
MAINTAINER Chris <36web.rocks@gmail.com>

RUN npm install -g backslide@1.2.1 \
    && npm cache clear \
	&& rm -rf /tmp/* \
    && mkdir /slide \
    && chown -R node:node /slide

USER node
WORKDIR /slide
VOLUME /slide
EXPOSE 4100

CMD [ "bs", "-h" ]
